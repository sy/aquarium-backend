-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- 主机： 127.0.0.1
-- 生成日期： 2021-10-24 20:49:07
-- 服务器版本： 10.5.5-MariaDB-log
-- PHP 版本： 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 数据库： `lab`
--

-- --------------------------------------------------------

--
-- 表的结构 `aquarium_command`
--

CREATE TABLE `aquarium_command` (
  `command_id` int(10) UNSIGNED NOT NULL,
  `device_id` char(32) NOT NULL,
  `command` varchar(64) NOT NULL,
  `param` varchar(255) NOT NULL,
  `status` varchar(10) NOT NULL,
  `create_time` datetime NOT NULL,
  `execute_time` datetime NOT NULL,
  `msg` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- 表的结构 `aquarium_device`
--

CREATE TABLE `aquarium_device` (
  `device_id` char(32) NOT NULL,
  `status` varchar(10) NOT NULL,
  `last_heartbeat_time` datetime NOT NULL,
  `ph` varchar(10) NOT NULL,
  `temperature` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- 转储表的索引
--

--
-- 表的索引 `aquarium_command`
--
ALTER TABLE `aquarium_command`
  ADD PRIMARY KEY (`command_id`),
  ADD KEY `device_id` (`device_id`);

--
-- 表的索引 `aquarium_device`
--
ALTER TABLE `aquarium_device`
  ADD PRIMARY KEY (`device_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
