<?php
/**
 * Device
 * 
 * @author ShuangYa
 * @package Example
 * @category Model
 * @link https://www.sylingd.com/
 * @copyright Copyright (c) 2019 ShuangYa
 */
namespace App\Model;

use Sy\ModelAbstract;

class Device extends ModelAbstract {
	protected $_table_name = 'aquarium_device';
	protected $_primary_key = 'device_id';
}