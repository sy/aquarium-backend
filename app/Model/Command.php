<?php
/**
 * Command
 * 
 * @author ShuangYa
 * @package Example
 * @category Model
 * @link https://www.sylingd.com/
 * @copyright Copyright (c) 2019 ShuangYa
 */
namespace App\Model;

use Sy\ModelAbstract;

class Command extends ModelAbstract {
	protected $_table_name = 'aquarium_command';
	protected $_primary_key = 'command_id';
}