<?php
/**
 * 配置
 * 
 * @author ShuangYa
 * @package Example
 * @category Configutation
 * @link https://www.sylingd.com/
 * @copyright Copyright (c) 2019 ShuangYa
 */
namespace App;

use App\Plugin\AppInterceptor;

class Configuration {
	public function setPlugin(AppInterceptor $app) {
		$app->register();
	}
}
