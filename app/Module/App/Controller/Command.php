<?php
/**
 * Command
 * 
 * @author ShuangYa
 * @package Example
 * @category Controller
 * @link https://www.sylingd.com/
 * @copyright Copyright (c) 2019 ShuangYa
 */
namespace App\Module\App\Controller;

use App\Library\Utils;
use App\Service\Command as ServiceCommand;
use Sy\ControllerAbstract;
use Sy\Http\Request;

class Command extends ControllerAbstract {
	private $command;

	public function __construct(ServiceCommand $command) {
		$this->command = $command;
	}

	public function executeAction(Request $request) {
		$id = $request->get['id'];
		$command = $request->get['command'];
		$param = $request->get['param'];

		if (!$param) {
			$param = '';
		}

		$id = $this->command->add([
			'device_id' => $id,
			'command' => $command,
			'param' => $param,
		]);

		return Utils::getResult([
			'id' => $id
		]);
	}

	public function statusAction(Request $request) {
		$id = $request->get['id'];
		return Utils::getResult($this->command->get($id));
	}

	public function logAction(Request $request) {
		$id = $request->get['id'];
		return Utils::getResult($this->command->list([
			'device_id' => $id
		]));
	}
}