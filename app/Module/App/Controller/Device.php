<?php
/**
 * Device
 * 
 * @author ShuangYa
 * @package Example
 * @category Controller
 * @link https://www.sylingd.com/
 * @copyright Copyright (c) 2019 ShuangYa
 */
namespace App\Module\App\Controller;

use App\Library\Utils;
use App\Model\Device as ModelDevice;
use Sy\ControllerAbstract;
use Sy\Http\Request;

class Device extends ControllerAbstract {
	private $device;

	public function __construct(ModelDevice $device) {
		$this->device = $device;
	}

	public function getAction(Request $request) {
		$id = $request->get['id'];

		$device = $this->device->get($id);

		if (!$device) {
			return Utils::getResult([
				'error' => '未找到设备'
			]);
		}

		// 检查上次心跳时间，如果大于15s则认为设备离线了
		$t = strtotime($device['last_heartbeat_time']);
		if (time() - $t > 15) {
			$this->device->set([
				'status' => 'offline'
			], $id);
			$device['status'] = 'offline';
		}

		return Utils::getResult($device);
	}
}