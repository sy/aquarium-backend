<?php
/**
 * Index
 * 
 * @author ShuangYa
 * @package Example
 * @category Controller
 * @link https://www.sylingd.com/
 * @copyright Copyright (c) 2019 ShuangYa
 */
namespace App\Module\App\Controller;

use App\Library\Utils;
use Sy\ControllerAbstract;

class Index extends ControllerAbstract {
	public function indexAction() {
		return Utils::getResult();
	}
}