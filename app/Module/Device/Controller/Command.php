<?php
/**
 * Command
 * 
 * @author ShuangYa
 * @package Example
 * @category Controller
 * @link https://www.sylingd.com/
 * @copyright Copyright (c) 2019 ShuangYa
 */
namespace App\Module\Device\Controller;

use App\Library\Utils;
use App\Service\Command as ServiceCommand;
use Sy\ControllerAbstract;
use Sy\Http\Request;

class Command extends ControllerAbstract {
	private $command;

	public function __construct(ServiceCommand $command) {
		$this->command = $command;
	}

	public function updateAction(Request $request) {
		$id = $request->get['id'];
		$command_id = $request->get['command_id'];
		$success = intval($request->get['success']);
		$msg = $request->get['msg'];

		$this->command->set([
			'device_id' => $id,
			'status' => $success === 1 ? ServiceCommand::STATUS_SUCCESS : ServiceCommand::STATUS_ERROR,
			'msg' => $msg,
			'execute_time' => date('Y-m-d H:i:s'),
		], $command_id);

		$res = Utils::getMachineResult('success');
		header('Content-Length: ' . strlen($res));
		return $res;
	}
}