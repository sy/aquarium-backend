<?php
/**
 * User Inject
 * 
 * @author ShuangYa
 * @package Plugin
 * @category Configutation
 * @link https://www.sylingd.com/
 * @copyright Copyright (c) 2019 ShuangYa
 */
namespace App\Plugin;

use Sy\Plugin;
use Sy\Http\Request;

class AppInterceptor {
	public function register() {
		Plugin::register('beforeDispatch', [$this, 'handle']);
	}

	public function handle(Request $request) {
		if (strtolower($request->module) === "app") {
			header('Content-Type: application/json; charset=UTF-8');
		}
		if (strtolower($request->module) === "device") {
			header('Content-Type: plain/text');
		}
	}
}